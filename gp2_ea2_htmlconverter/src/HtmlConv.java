import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


/**
 * This calls reads a text from the file datei1702_vor.html. Inside the program
 * all Umlaute will be replaced by its HTML-Entity.
 * After that, the text will be writen back to the file datei1702_nach.html
 * 
 * @author Marc Fischer
 * 
 * @version 1.0.0
 * 
 * This is a programming exercise for -- Grundlagen der Programmierung II --
 *
 */
public class HtmlConv {
    
    private List<String> allLinesFromFile;
 
    /**
     * 
     * @param file
     * @return
     */
    private List<String> readInFromFile(File file) {
    	List<String> allRowsFromFile = new ArrayList<String>();
    	
        String line = null;
        if (!file.canRead() || !file.isFile()) {
        	System.err.println("File cannot be read!");
            System.exit(0);
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.ISO_8859_1))) {
            while ((line = br.readLine()) != null) {
                allRowsFromFile.add(line);
                allRowsFromFile.add("\n");
            }
        } catch (IOException e) {
            System.err.println("Couldn't read from file " + file);
            e.printStackTrace();
        }
        return allRowsFromFile;
    }
    
    
    /**
     * 
     * @param oldList
     * @return
     */
	private List<String> replaceAllUmlaute(List<String> oldList) {
		List<String> newList = new ArrayList<String>();
		String newLine = null;
		
		for (String oldLine : oldList) {
    		newLine = oldLine.replaceAll("Ä", "&Auml;");
    		newLine = newLine.replace("ä", "&auml;");
    		newLine = newLine.replace("Ö", "&Ouml;");
    		newLine = newLine.replace("ö", "&ouml;");
    		newLine = newLine.replace("Ü", "&Uuml;");
    		newLine = newLine.replace("ü", "&uuml;");
    		newLine = newLine.replace("ß", "&szlig;");
			newList.add(newLine);
		}
		return newList;
	}
    
    
	/**
	 * 
	 * @param file
	 * @param listToWrite
	 */
    public void writeIntoFile(final File file, final List<String> listToWrite) {
    	try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (String line: listToWrite) {
				bw.write(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }


    /**
     * 
     * @throws IOException
     */
    public void run() throws IOException {

        File fileToRead = new File("datei1702_vor.html");

        File fileToWrite = new File("datei1702_nach.html");

        allLinesFromFile = replaceAllUmlaute(readInFromFile(fileToRead));

        writeIntoFile(fileToWrite, allLinesFromFile);
    }


    /**
     * This is the main-Method, the entry point of the application.
     * 
     * @param args 
     * 
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        HtmlConv htmlConv = new HtmlConv();
        htmlConv.run();

    }
}
